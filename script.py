
movies = [
    {
        'name': 'Interstellar',
        'ratings': {
            'John': 10,
            'Jack': 3
        }
    },
    {
        'name': 'Avengers: Infinity War',
        'ratings': {
            'Jack': 9,
            'Jane': 10
        }
    },
    {
        'name': 'Avengers: Infinity War 2',
        'ratings': {

        }
    }
]


def add_movie_command():
    movieName = input("Введите название фильма: >").split().lover()
    if movieName in movies['name'].values():
        print("Такой фильм уже существует")
    else:
        movie = {'name': movieName, 'ratings': {}}
        movies.append(movie)


def show_movie(movie, detailing):
    if (len(movie.get('ratings').keys())) > 0:
        if detailing == 'full':
            print(movie['name'])
            for rate in movie['ratings']:
                print('{:<15}{:<15}'.format(rate, movie['ratings'].get(rate)))
        else:
            print('{:<30}{:<15}'.format(movie['name'],
                                        float(sum(movie.get('ratings').values()) / len(movie.get('ratings').keys()))))
    else:
        print('{:<30}{:<15}'.format(movie['name'], 'Не оценивался'))


def show_movie_list_command():
    for movie in movies:
        show_movie(movie, 'short')


def find_movie_command():
    movieName = input("Введите название фильма: >").split().lover()
    finded_movie = ""
    for movie in movies:
        if movie['name'].upper() == movieName.upper():
            finded_movie = movie
            break
        else:
            print('Фильм с таким названием не найден')
            break
    if finded_movie != "":
        show_movie(finded_movie, 'full')


def remove_movie_command():
    movieName = input("Введите название фильма: >").split().lover()
    i = 0
    for movie in movies:
        if movie['name'].upper() == movieName.upper():
            if movies.pop(i) != "":
                print(f"Вы удалили фильм {movieName}")
                break
        else:
            print("Фильма с таким названием не существует!")
            break
        i += 1

def command_exit():
    exit('До свидания')

def rate_movie_command():
    movieName = input("Введите название фильма: >").split().lover()
    rateName = input("Введите свое имя: >").split().lover()
    rate = float(input("Введите оценку: >"))
    i = 0
    if rate > 10 or rate < 0:
        return 
    for movie in movies:
        if movie['name'].upper() == movieName.upper():
            if movie['name'] != "":
                movie['ratings'][rateName] = rate
                show_movie(movie, 'full')
                break
        else:
            print("Фильма с таким названием не существует!")
            break
        i += 1


commands = {
    'add': add_movie_command,
    'delete': remove_movie_command,
    'rate': rate_movie_command,
    'list': show_movie_list_command,
    'find': find_movie_command,
    'exit': command_exit
}
while True:
    command = input('Введите дальнейшее действие: add, delete, rate, list, find, exit  > ')
    commands[command]()
